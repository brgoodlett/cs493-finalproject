const { StatusCodes } = require("http-status-codes");

module.exports = class LoadsService {
  constructor() {
    this.db = require("./DB.service");
  }

  async create_new_load(load) {
    let result = await this.db.awaitQuery(
      `INSERT INTO Loads(description, weight, deliver_by) VALUES (?,?,?)`,
      [load.description, load.weight, load.deliver_by]
    );
    return result;
  }

  async get_load_count() {
    let result = await this.db.awaitQuery(`SELECT COUNT(*) FROM Loads`);
    return result[0]["COUNT(*)"];
  }

  async get_all_loads(page_size = 5, page = 0) {
    let result = await this.db.awaitQuery(
      `SELECT 
	    l.deliver_by, l.description, l.load_id, l.weight, b.boat_id
      FROM 
	    Loads l
      LEFT JOIN boat_carries_load b
      ON l.load_id = b.load_id 
      LIMIT ? OFFSET ?`,
      [parseInt(page_size), parseInt(page)]
    );
    return result;
  }

  async get_load_by_id(load_id) {
    let result = await this.db.awaitQuery(
      `SELECT 
	    l.deliver_by, l.description, l.load_id, l.weight, b.boat_id
      FROM 
	    Loads l
      LEFT JOIN boat_carries_load b
      ON l.load_id = b.load_id
      WHERE l.load_id = ?`,
      [load_id]
    );
    return result[0];
  }

  async remove_load_from_boat(load_id, boat_id) {
    let result = await this.db.awaitQuery(
      `DELETE FROM boat_carries_load WHERE load_id = ? and boat_id = ?`,
      [load_id, boat_id]
    );
    return result;
  }

  async delete_load_by_id(load_id) {
    let result = await this.db.awaitQuery(
      `DELETE FROM Loads WHERE load_id = ?`,
      [load_id]
    );
    return result;
  }

  async get_all_loads_by_boat_id(boat_id) {
    let result = await this.db.awaitQuery(
      `SELECT 
	    l.deliver_by, l.description, l.load_id, l.weight, b.boat_id
      FROM 
	    Loads l
      LEFT JOIN boat_carries_load b
      ON l.load_id = b.load_id
      WHERE b.boat_id = ?`,
      [boat_id]
    );
    return result;
  }

  async modify_load(load_id, props) {
    let sql_params = "";
    Object.keys(props).forEach((key) => {
      sql_params += `${key} = \"${props[key]}\", `;
    });
    let sql = `UPDATE Loads 
    SET ${sql_params.trim().replace(/\,$/, "")}
    WHERE load_id = ?`;
    let result = await this.db.awaitQuery(sql, [load_id]);
    return result;
  }

  async confirm_load_exists(req, res, next) {
    let result = await this.db.awaitQuery(
      "SELECT load_id FROM Loads WHERE load_id = ?",
      [req.params.load_id]
    );
    if (result.length > 0) {
      next();
    } else {
      return res
        .status(StatusCodes.NOT_FOUND)
        .send({ message: "load not found" });
    }
  }

  async confirm_load_loaded(req, res, next) {
    let result = await this.db.awaitQuery(
      "SELECT load_id FROM boat_carries_load WHERE load_id = ? AND boat_id = ?",
      [req.params.load_id, req.params.boat_id]
    );
    if (result.length > 0) {
      next();
    } else {
      return res
        .status(StatusCodes.BAD_REQUEST)
        .send({ message: "load not loaded" });
    }
  }

  async confirm_load_not_loaded(req, res, next) {
    let result = await this.db.awaitQuery(
      "SELECT load_id FROM boat_carries_load WHERE load_id = ?",
      [req.params.load_id]
    );
    if (result.length === 0) {
      next();
    } else {
      return res
        .status(StatusCodes.BAD_REQUEST)
        .send({ message: "load already loaded" });
    }
  }
};
