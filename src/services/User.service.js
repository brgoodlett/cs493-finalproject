module.exports = class UserService {
  constructor() {
    this.db = require("./DB.service");
  }

  async get_single_user_by_username(username) {
    let rows = await this.db.awaitQuery(
      `SELECT * FROM Users WHERE username = ?`,
      [username]
    );
    if (rows[0] === undefined) throw Error("User not found");
    return rows[0];
  }

  async get_all_users() {
    let rows = await this.db.awaitQuery(
      "SELECT user_id, username FROM Users ORDER BY user_id",
      []
    );
    if (!rows) throw Error("Error getting all users");
    return rows;
  }

  async create_single_user(username, password_hash) {
    let rows = await this.db.awaitQuery(`CALL create_user(?, ?);`, [
      username,
      password_hash,
    ]);
    if (rows[0][0].user_id === undefined) throw Error("Error creating user");
    else return rows[0][0].user_id;
  }
};
