const { StatusCodes } = require("http-status-codes");

module.exports = class BoatsService {
  constructor() {
    this.db = require("./DB.service");
  }

  async create_new_boat(boat) {
    let rows = await this.db.awaitQuery(
      `INSERT INTO Boats (owner_id, boat_name, boat_type, length) VALUES (?, ?, ?, ?);`,
      [boat.owner_id, boat.boat_name, boat.boat_type, boat.length]
    );
    if (rows.insertId === undefined) throw Error("Error creating user");
    else return rows.insertId;
  }

  async get_boat_count_by_id(owner_id) {
    let result = await this.db.awaitQuery(
      `SELECT COUNT(*) FROM Boats where Boats.owner_id = ?`,
      [owner_id]
    );
    return result[0]["COUNT(*)"];
  }

  async get_boat_by_id(boat_id) {
    //TODO: add loads to query result
    let rows = await this.db.awaitQuery(
      `SELECT Boats.boat_id, Boats.owner_id, Boats.boat_name, Boats.boat_type, Boats.length, l.loads
      FROM Boats 
      LEFT JOIN (
        SELECT boat_id, JSON_ARRAYAGG(load_id) loads
        FROM boat_carries_load
        GROUP BY boat_id 
      ) l 
      ON Boats.boat_id = l.boat_id 
      WHERE Boats.boat_id = ?`,
      [boat_id]
    );
    rows.forEach((row) => {
      if (row.loads) {
        row.loads = JSON.parse(row.loads);
      } else {
        row.loads = [];
      }
    });
    return rows[0];
  }

  async get_all_user_boats(user_id, page = 0, page_size = 5) {
    let rows = await this.db.awaitQuery(
      `SELECT Boats.boat_id, Boats.owner_id, Boats.boat_name, Boats.boat_type, Boats.length, l.loads
      FROM Boats 
      LEFT JOIN (
        SELECT boat_id, JSON_ARRAYAGG(load_id) loads
        FROM boat_carries_load
        GROUP BY boat_id 
      ) l 
      ON Boats.boat_id = l.boat_id 
      WHERE Boats.owner_id = ?
      LIMIT ? OFFSET ?`,
      [user_id, page_size, page * page_size]
    );
    rows.forEach((row) => {
      if (row.loads) {
        row.loads = JSON.parse(row.loads);
      } else {
        row.loads = [];
      }
    });

    return rows;
  }

  async get_owner_id(boat_id) {
    let data = await this.db.awaitQuery(
      `select owner_id from Boats where boat_id = ?`,
      [boat_id]
    );
    return data[0];
  }

  async delete_boat_by_id(boat_id) {
    let result = await this.db.awaitQuery(
      `DELETE FROM Boats WHERE boat_id = ?`,
      [boat_id]
    );
    return result;
  }

  async modify_boat(boat_id, props) {
    let sql_params = "";
    Object.keys(props).forEach((key) => {
      sql_params += `${key} = \"${props[key]}\", `;
    });
    let sql = `UPDATE Boats 
    SET ${sql_params.trim().replace(/\,$/, "")}
    WHERE boat_id = ?`;
    let result = await this.db.awaitQuery(sql, [boat_id]);
    return result;
  }

  async add_load_to_boat(boat_id, load_id) {
    let result = await this.db.awaitQuery(
      `INSERT INTO boat_carries_load(boat_id, load_id) VALUES (?, ?)`,
      [boat_id, load_id]
    );
    return result;
  }

  async confirm_user_owns_boat(req, res, next) {
    let data = undefined;
    try {
      data = await this.get_owner_id(req.params.boat_id);
    } catch (err) {
      return res.status(StatusCodes.INTERNAL_SERVER_ERROR).send();
    }
    if (data === undefined) {
      return res
        .status(StatusCodes.NOT_FOUND)
        .send({ message: "boat not found" });
    }
    if (data.owner_id !== req.token_data.user_id) {
      return res
        .status(StatusCodes.FORBIDDEN)
        .send({ message: "you do not own this boat" });
    }
    next();
  }
};
