let { StatusCodes } = require("http-status-codes");

module.exports = class UtilityService {
  constructor() {}

  enforce_accepts_json(req, res, next) {
    let accepts = req.headers.accept;
    if (accepts === undefined || accepts.toLowerCase() !== "application/json") {
      return res.status(StatusCodes.NOT_ACCEPTABLE).send({
        message: "unacceptable return type. must be 'application/json'",
      });
    } else {
      next();
    }
  }

  method_not_allowed = (req, res) => {
    res
      .status(StatusCodes.METHOD_NOT_ALLOWED)
      .send({ message: "Method not allowed" });
  };
};
