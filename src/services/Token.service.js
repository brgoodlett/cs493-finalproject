const jwt = require("jsonwebtoken");
const { StatusCodes } = require("http-status-codes");

module.exports = class TokenService {
  constructor() {}

  generate_token(user) {
    return jwt.sign(
      {
        username: user.username,
        user_id: user.user_id,
      },
      process.env.JWT_SECRET
    );
  }

  //token validation middleware
  authenticate_token(req, res, next) {
    try {
      let token = req.headers.authorization.split(" ")[1];
      let data = jwt.verify(token, process.env.JWT_SECRET);
      req.token_data = data;
      next();
    } catch (err) {
      return res
        .status(StatusCodes.UNAUTHORIZED)
        .send({ message: "Authentication required" });
    }
  }
};
