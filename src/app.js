const express = require("express"),
  app = express(),
  path = require("path"),
  fs = require("fs");

app.use(express.json());

// Example Error handler copied from:
// https://www.npmjs.com/package/express-joi-validation#error-handling
const joi_err_handler = (err, req, res, next) => {
  if (err && err.error && err.error.isJoi) {
    // we had a joi error, let's return a custom 400 json response
    res.status(400).send({
      type: err.type, // will be "query", "headers", "body", or "params"
      message: err.error.toString(),
    });
  } else {
    // pass on to another error handler
    next(err);
  }
};

//include all routes and mount with the first part of their file name
const dir = path.join(__dirname, "routers");
fs.readdir(dir, (err, files) => {
  if (err) return console.log(err);
  files.forEach((file) => {
    let routerName = file.split(".")[0];
    let router = require(path.join(dir, file));
    router.use(joi_err_handler);
    app.use(`/${routerName}`, router);
  });
});

//app.listen(port, () => console.log("Listening on port: " + port));
module.exports = app;
