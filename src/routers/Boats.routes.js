// Import libraries
const express = require("express");
const validator = require("express-joi-validation").createValidator({
  passError: true,
});
const { StatusCodes } = require("http-status-codes");

// Import Schemas
let { boatSchema, partialBoatSchema } = require("../schemas/Boats.schema");

// Import Services
let TokenService = require("../services/Token.service");
let UtilityService = require("../services/utility.service");
let BoatService = require("../services/Boats.service");
let LoadService = require("../services/Loads.service");

let router = express.Router();
let tokenService = new TokenService();
let utilityService = new UtilityService();
let boatService = new BoatService();
let loadService = new LoadService();

// create a new boat
router.post(
  "/",
  [
    utilityService.enforce_accepts_json,
    tokenService.authenticate_token,
    validator.body(boatSchema),
  ],
  async (req, res) => {
    let boat = req.body;
    boat.owner_id = req.token_data.user_id;
    let boat_id = null;
    try {
      boat_id = await boatService.create_new_boat(boat);
    } catch (err) {
      return res
        .status(StatusCodes.INTERNAL_SERVER_ERROR)
        .send({ message: err.message });
    }

    boat.boat_id = boat_id;
    boat.self = `${process.env.APP_URL}/boats/${boat.boat_id}`;

    return res.status(StatusCodes.CREATED).send(boat);
  }
);

// get list of all boats, with pagination
router.get(
  "/",
  [utilityService.enforce_accepts_json, tokenService.authenticate_token],
  async (req, res) => {
    let rows = [];

    let page = 0;
    let page_size = 5;
    let next = "";

    if (req.query.page) page = parseInt(req.query.page);
    if (req.query.page_size) page_size = parseInt(req.query.page_size);
    let count = 0;
    try {
      rows = await boatService.get_all_user_boats(
        req.token_data.user_id,
        page,
        page_size
      );
      if (rows === undefined || rows.length === 0) {
        return res
          .status(StatusCodes.NOT_FOUND)
          .send({ message: "User has no boats" });
      }
      count = await boatService.get_boat_count_by_id(req.token_data.user_id);
      rows.forEach((el) => {
        el.self = `${process.env.APP_URL}/boats/${el.boat_id}`;
      });

      if (rows.length < page_size) {
        next = "NO MORE PAGES";
      } else {
        next = `${process.env.APP_URL}/boats?page=${
          page + 1
        }&page_size=${page_size}`;
      }
    } catch (err) {
      console.log(err);
      return res
        .status(StatusCodes.NOT_FOUND)
        .send({ message: "User has no boats" });
    }
    return res
      .status(StatusCodes.OK)
      .send({ boats: rows, count: count, next: next });
  }
);

// get a single boat by id
router.get(
  "/:boat_id",
  [
    utilityService.enforce_accepts_json,
    tokenService.authenticate_token,
    boatService.confirm_user_owns_boat.bind(boatService),
  ],
  async (req, res) => {
    try {
      let boat = await boatService.get_boat_by_id(req.params.boat_id);
      boat.self = `${process.env.APP_URL}/boats/${boat.boat_id}`;
      return res.status(StatusCodes.OK).send(boat);
    } catch (err) {
      return res
        .status(StatusCodes.NOT_FOUND)
        .send({ message: "boat not found" });
    }
  }
);

// add a load to the boat
router.post(
  "/:boat_id/loads/:load_id",
  [
    tokenService.authenticate_token,
    // TODO: clean this up so there aren't so many queries being made.
    boatService.confirm_user_owns_boat.bind(boatService),
    loadService.confirm_load_exists.bind(loadService),
    loadService.confirm_load_not_loaded.bind(loadService),
  ],
  async (req, res) => {
    try {
      let result = await boatService.add_load_to_boat(
        req.params.boat_id,
        req.params.load_id
      );
      return res.status(StatusCodes.NO_CONTENT).send();
    } catch (err) {
      return res.status(StatusCodes.INTERNAL_SERVER_ERROR).send();
    }
  }
);

// remove a load from the boat
router.delete(
  "/:boat_id/loads/:load_id",
  [
    tokenService.authenticate_token,
    boatService.confirm_user_owns_boat.bind(boatService),
    loadService.confirm_load_loaded.bind(loadService),
  ],
  async (req, res) => {
    try {
      let result = await loadService.remove_load_from_boat(
        req.params.load_id,
        req.params.boat_id
      );
      return res.status(StatusCodes.NO_CONTENT).send();
    } catch (err) {
      return res.status(StatusCodes.INTERNAL_SERVER_ERROR).send();
    }
  }
);

// get all loads associated with a boat
router.get(
  "/:boat_id/loads",
  [
    utilityService.enforce_accepts_json,
    tokenService.authenticate_token,
    boatService.confirm_user_owns_boat.bind(boatService),
  ],
  async (req, res) => {
    try {
      let result = await loadService.get_all_loads_by_boat_id(
        req.params.boat_id
      );
      if (result.length === 0) {
        return res
          .status(StatusCodes.NOT_FOUND)
          .send({ message: "loads not found" });
      }
      result.forEach((load) => {
        load.self = `${process.env.APP_URL}/loads/${load.load_id}`;
      });
      return res.status(StatusCodes.OK).send(result);
    } catch (err) {
      return res.status(StatusCodes.INTERNAL_SERVER_ERROR).send();
    }
  }
);

// update a boat using partial boat data
router.patch(
  "/:boat_id",
  [
    tokenService.authenticate_token,
    validator.body(partialBoatSchema),
    boatService.confirm_user_owns_boat.bind(boatService),
  ],
  async (req, res) => {
    try {
      await boatService.modify_boat(req.params.boat_id, req.body);
      return res.status(StatusCodes.NO_CONTENT).send();
    } catch (err) {
      return res.status(StatusCodes.INTERNAL_SERVER_ERROR).send();
    }
  }
);

// update a boat using a full boat (excluding boat_id and owner_id)
router.put(
  "/:boat_id",
  [
    tokenService.authenticate_token,
    validator.body(boatSchema),
    boatService.confirm_user_owns_boat.bind(boatService),
  ],
  async (req, res) => {
    try {
      await boatService.modify_boat(req.params.boat_id, req.body);
      return res.status(StatusCodes.NO_CONTENT).send();
    } catch (err) {
      return res.status(StatusCodes.INTERNAL_SERVER_ERROR).send();
    }
  }
);

// delete a boat by id
router.delete(
  "/:boat_id",
  [
    tokenService.authenticate_token,
    boatService.confirm_user_owns_boat.bind(boatService),
  ],
  async (req, res) => {
    try {
      await boatService.delete_boat_by_id(req.params.boat_id);
      return res.status(StatusCodes.NO_CONTENT).send();
    } catch (err) {
      return res.status(StatusCodes.INTERNAL_SERVER_ERROR).send();
    }
  }
);

// 405 Errors
router.all("/", utilityService.method_not_allowed);
router.all("/:boat_id", utilityService.method_not_allowed);
router.all("/:boat_id/loads", utilityService.method_not_allowed);
router.all("/:boat_id/loads/:load_id", utilityService.method_not_allowed);
module.exports = router;
