// Import libraries
const express = require("express"),
  bcrypt = require("bcrypt"),
  validator = require("express-joi-validation").createValidator({
    passError: true,
  }),
  { StatusCodes } = require("http-status-codes");

// Import Schemas
let { CreateUserSchema } = require("../schemas/User.schema");

// Import Services
let UserService = require("../services/User.service"),
  TokenService = require("../services/Token.service"),
  UtilityService = require("../services/utility.service");

let router = express.Router(),
  userService = new UserService(),
  tokenService = new TokenService(),
  utilityService = new UtilityService();

//Create a user
router.post(
  "/",
  [utilityService.enforce_accepts_json, validator.body(CreateUserSchema)],
  async (req, res) => {
    let { username, password } = req.body;
    try {
      let hash = await bcrypt.hash(password, 10);
      let user_id = await userService.create_single_user(username, hash);
      return res.status(StatusCodes.CREATED).send({ user_id: user_id });
    } catch (err) {
      if (err.code === "ER_DUP_ENTRY") {
        return res
          .status(StatusCodes.BAD_REQUEST)
          .send({ message: `Username ${username} taken` });
      }
      return res
        .status(StatusCodes.INTERNAL_SERVER_ERROR)
        .send({ message: "Failed to create user" });
    }
  }
);

// get list of all users without pagination
router.get("/", [utilityService.enforce_accepts_json], async (req, res) => {
  try {
    let users = await userService.get_all_users();
    return res.status(StatusCodes.OK).send({ users: users });
  } catch (err) {
    return res
      .status(StatusCodes.NOT_FOUND)
      .send({ message: "no users found" });
  }
});

// create a token for the user
router.post(
  "/token",
  [utilityService.enforce_accepts_json, validator.body(CreateUserSchema)],
  async (req, res) => {
    //if there is no token, try to log the user in.
    try {
      CreateUserSchema.validate(req.body);
      let { username, password } = req.body;
      let user = await userService.get_single_user_by_username(username);
      if (bcrypt.compareSync(password, user.password)) {
        let token = tokenService.generate_token(user);
        return res
          .status(StatusCodes.CREATED)
          .send({ Token: token, user_id: user.user_id });
      }
    } catch (err) {
      return res.status(StatusCodes.NOT_FOUND).send({ message: err.message });
    }
    return res
      .status(StatusCodes.BAD_REQUEST)
      .send({ message: "Invalid username or password" });
  }
);

router.all("/", utilityService.method_not_allowed);
router.all("/token", utilityService.method_not_allowed);
module.exports = router;
