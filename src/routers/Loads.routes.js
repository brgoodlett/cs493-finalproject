// Import libraries
const express = require("express");
const validator = require("express-joi-validation").createValidator({
  passError: true,
});
const { StatusCodes } = require("http-status-codes");

// Import Schemas
let { loadSchema, partialLoadSchema } = require("../schemas/Loads.schema");

// Import Services
let TokenService = require("../services/Token.service");
let UtilityService = require("../services/utility.service");
let LoadService = require("../services/Loads.service");

let router = express.Router();
let tokenService = new TokenService();
let utilityService = new UtilityService();
let loadService = new LoadService();

router.post(
  "/",
  [
    utilityService.enforce_accepts_json,
    tokenService.authenticate_token,
    validator.body(loadSchema),
  ],
  async (req, res) => {
    let load = req.body;
    try {
      let result = await loadService.create_new_load(load);
      load.load_id = result.insertId;
      load.boat_id = null;
      load.self = `${process.env.APP_URL}/loads/${load.load_id}`;
      return res.status(StatusCodes.CREATED).send(load);
    } catch (err) {
      console.log(err);
      return res.status(StatusCodes.INTERNAL_SERVER_ERROR).send();
    }
  }
);

router.get(
  "/",
  [utilityService.enforce_accepts_json, tokenService.authenticate_token],
  async (req, res) => {
    let page_size = req.query.page_size || 5;
    let page = req.query.page || 0;

    try {
      let result = {};
      result.loads = await loadService.get_all_loads(page_size, page);
      result.count = await loadService.get_load_count();
      if (result.loads.length === 0)
        return res
          .status(StatusCodes.NOT_FOUND)
          .send({ message: "loads not found" });

      result.loads.forEach(
        (load) => (load.self = `${process.env.APP_URL}/loads/${load.load_id}`)
      );

      if (result.loads.length < page_size) {
        result.next = "NO MORE PAGES";
      } else {
        result.next = `${
          process.env.APP_URL
        }/loads?page_size=${page_size}&page=${page + 1}`;
      }
      res.status(StatusCodes.OK).send(result);
    } catch (err) {
      console.log(err);
      return res.status(500).send(err);
    }
  }
);

router.get(
  "/:load_id",
  [
    utilityService.enforce_accepts_json,
    tokenService.authenticate_token,
    loadService.confirm_load_exists.bind(loadService),
  ],
  async (req, res) => {
    try {
      let load = await loadService.get_load_by_id(req.params.load_id);
      load.self = `${process.env.APP_URL}/loads/${load.load_id}`;
      return res.status(StatusCodes.OK).send(load);
    } catch (err) {
      console.log(err);
      return res.status(StatusCodes.INTERNAL_SERVER_ERROR).send(err);
    }
  }
);

router.patch(
  "/:load_id",
  [
    tokenService.authenticate_token,
    validator.body(partialLoadSchema),
    loadService.confirm_load_exists.bind(loadService),
  ],
  async (req, res) => {
    try {
      await loadService.modify_load(req.params.load_id, req.body);
      return res.status(StatusCodes.NO_CONTENT).send();
    } catch (err) {
      console.log(err);
      return res.status(StatusCodes.INTERNAL_SERVER_ERROR).send(err);
    }
  }
);

router.put(
  "/:load_id",
  [
    tokenService.authenticate_token,
    validator.body(loadSchema),
    loadService.confirm_load_exists.bind(loadService),
  ],
  async (req, res) => {
    try {
      await loadService.modify_load(req.params.load_id, req.body);
      return res.status(StatusCodes.NO_CONTENT).send();
    } catch (err) {
      console.log(err);
      return res.status(StatusCodes.INTERNAL_SERVER_ERROR).send(err);
    }
  }
);

router.delete(
  "/:load_id",
  [
    tokenService.authenticate_token,
    loadService.confirm_load_exists.bind(loadService),
  ],
  async (req, res) => {
    try {
      let result = await loadService.delete_load_by_id(req.params.load_id);
      return res.status(StatusCodes.NO_CONTENT).send();
    } catch (err) {
      console.log(err);
      return res.status(StatusCodes.INTERNAL_SERVER_ERROR).send(err);
    }
  }
);

router.all("/", utilityService.method_not_allowed);
router.all("/:load_id", utilityService.method_not_allowed);
module.exports = router;
