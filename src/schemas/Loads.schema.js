const Joi = require("joi");

let partialLoadSchema = Joi.object({
  description: Joi.string().regex(/^(.){1,100}$/),
  weight: Joi.number().integer().min(1).max(9999999999),
  deliver_by: Joi.date().raw(),
}).or("description", "weight", "deliver_by");

let loadSchema = Joi.object({
  description: Joi.string()
    .regex(/^(.){1,100}$/)
    .required(),
  weight: Joi.number().integer().min(1).max(9999999999).required(),
  deliver_by: Joi.date().raw().required(),
});

module.exports = {
  partialLoadSchema,
  loadSchema,
};
