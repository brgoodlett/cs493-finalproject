const Joi = require("joi");

const CreateUserSchema = Joi.object({
  username: Joi.string()
    .regex(/^[\d\w]{4,45}$/)
    .required(),
  password: Joi.string()
    .regex(/^[\s\d\w]{6,60}$/)
    .required(),
});

module.exports = {
  CreateUserSchema,
};
