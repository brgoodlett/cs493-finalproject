const Joi = require("joi");

let partialBoatSchema = Joi.object({
  boat_name: Joi.string().regex(/^[\d\w\s]{4,45}$/),
  boat_type: Joi.string().regex(/^[\d\w\s]{4,45}$/),
  length: Joi.number().integer().min(1).max(9999999999),
}).or("boat_name", "boat_type", "length");

let boatSchema = Joi.object({
  boat_name: Joi.string()
    .regex(/^[\d\w\s]{4,45}$/)
    .required(),
  boat_type: Joi.string()
    .regex(/^[\d\w\s]{4,45}$/)
    .required(),
  length: Joi.number().integer().min(1).max(9999999999).required(),
});

module.exports = {
  partialBoatSchema,
  boatSchema,
};
