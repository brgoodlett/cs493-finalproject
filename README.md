# goodletb - Final project API Documentation

## App URL: https://goodletb-final.wl.r.appspot.com/

## Account Creation / Login URL: none

I didn't implement a frontent page. There are requests in the postman collection that I created which will automatically create two users and log into them, storing their jwt in the {{jwt1}} and {{jwt2}} variables.

# Data Model

## User

| Property | Type   | Required | Accepted Values                           |
| -------- | ------ | -------- | ----------------------------------------- |
| user_id  | int    | auto     | na                                        |
| username | string | yes      | min=4, max=45, match=`/^[\d\w]{4,45}$/`   |
| password | string | yes      | min=6, max=60, match=`/^[\s\d\w]{6,60}$/` |

## Boat

| Property  | Type   | Required | Accepted Values                           |
| --------- | ------ | -------- | ----------------------------------------- |
| boat_id   | int    | auto     | na                                        |
| owner_id  | int    | yes      | min=1, max=9999999999                     |
| boat_name | string | yes      | min=4, max=45, match=`/^[\d\w\s]{4,45}$/` |
| boat_type | string | yes      | min=1, max=45, match=`/^[\d\w\s]{4,45}$/` |
| length    | int    | yes      | min=0, max= 9999999999                    |

## Load

| Property    | Type   | Required | Accepted Values                        |
| ----------- | ------ | -------- | -------------------------------------- |
| load_id     | int    | auto     | na                                     |
| description | string | yes      | min=1, max=100, match=`/^(.){1,100}$/` |
| weight      | int    | yes      | min=1, max=9999999999                  |
| deliver_by  | date   | yes      | 'yyyy-mm-dd'                           |

---

## Description of relationship between entities

A user can own one or more boats. Once they log in, any boat they create will automatically have their id as the owner_id. They can also create loads, however, these have no relationship to the user and anybody can edit them (I would never do this in a real api). The loads can be loaded into a boat, which is modeled in the database as a table with the boat_id and load_id to show that load_id is on boat_id. The user does not see this table, but sees the boat_id when requesting loads, and the load_ids that are on a boat when requesting them. Boats can be unloaded as well, and will automatically be unloaded on the deletion of a boat. When deleting a load, it will automatically be removed from any boats.

## Description of user model

For my user_id, I simply used the id that my database assigned to the row when inserting the new user. This made it easy to guarantee that I would never have a duplicate ID and didn't have to worry about generating new ones. When a user is created, the user sends a post request to the /Users route with a body using the following format:

```JSON
{
    "username":"example_username",
    "password":"example_password"
}
```

When the user has been created, a user can log into the user by making a post request to /Users/token, using the same body as before to supply the username and password they would like to use for authentication. Once they have been authenticated, they are given a jwt that has their user_id in it for authentication on future requests. For all requests other than the users and token post routes, all requests will be authenticated using a json web token. The user only needs to login once and then they will be able to pass the jwt for all future requests. This jwt contains the user's user_id inside, so the server can know which user it is interacting with without having to make a request to the database and lookup the name. When a boat is created, the user_id is used to set the owner_id of the boat. This is how the user gets their relationship to the boat. When a user requests all boats, it will filter only the boats that have an owner_id that matches the supplied jwt's user_id field.

## Other Notes

- In the following API spec, I did not specify the 405 or 406 returns for the routes. The API does return these errors in the appropriate situations, and there are a few tests in my postman collection which demonstrate this. Below are examples of the 405 and 406 errors.

### `405 - Method Not Allowed`

```JSON
{
    "message": "Method not allowed"
}
```

### `406 - Not Acceptable`

```JSON
{
    "message": "unacceptable return type. must be 'application/json'"
}
```

- I also did not implement the front end to create a user, but there are requests in my postman collection which will automatically create two users, log into them, and assign their tokens to {{jwt1}} and {{jwt2}} in the postman environment.
- The usernames in the postman collection are randomly generated, so if you see usernames like "bNEq7JnY8rhhjdoZOgG4r9lpJdgTaRdqXlVWzflMEyW3", this is totally expected and they are not modified in any way from what was sent to the server.
- Finally, I only showed one instance of an invalid jwt, but literally every route that is protected by a jwt uses the same code to verify the jwt is there, and valid. This means that it will work on any route that is protected by a jwt.

# Routes

## Users

# POST /users

## Description

Takes a username and password from the request body and creates a new user. Returns the user_id of the newly created user.

# Request Headers

```JSON
None
```

## Request Parameters

```
None
```

## Request Body

Required

```JSON
{
    "username": "example_username",
    "password": "example_password"
}
```

## Request Body Format

```
application/json
```

## Request JSON Attributes

| Name     | Type   | Description                 | Required? |
| -------- | ------ | --------------------------- | --------- |
| username | string | username of the new account | yes       |
| password | string | password of the new account | yes       |

## Response Body Format

```
application/json
```

## Response Status Codes

| Outcome | Status Code       | Notes                                                         |
| ------- | ----------------- | ------------------------------------------------------------- |
| Success | `201 Created`     | returns user_id of the newly created user                     |
| Failure | `400 Bad Request` | Fails if the input is invalid, or if the user already exists. |

## Response Body Examples

### Success - `201 Created`

```JSON
{
    "user_id": 558
}
```

### Failure - `400 Bad Request`

```JSON
{
    "type": "body",
    "message": "ValidationError: \"password\" is required"
}
```

### Failure - `400 Bad Request`

```JSON
{
    "message": "Username hjHAgMjwhEEBnOof11DWjDNZF8dghzO1jTnGlTc5OQVx taken"
}
```

# GET /users

## Description

An unprotected route to provide a list of all users. This is something that would not be good to implement in production and is only added to meet the requirements of the assignment.

# Request Headers

```JSON
None
```

## Request Parameters

```
None
```

## Request Body

```
None
```

## Request Body Format

```
None
```

## Request JSON Attributes

```
None
```

## Response Body Format

```
application/json
```

## Response Status Codes

| Outcome | Status Code     | Notes                                                                  |
| ------- | --------------- | ---------------------------------------------------------------------- |
| Success | `200 OK`        |                                                                        |
| Failure | `404 Not Found` | Fails if the database has no Users. Obviously this won't happen often. |

## Response Body Examples

### Success - `200 OK`

```JSON
{
    "users": [
        {
            "user_id": 1,
            "username": "test_user"
        },
        {
            "user_id": 2,
            "username": "test_user1"
        },
        {
            "user_id": 3,
            "username": "test_user3"
        }
    ]
}
```

### Failure - `404 Not Found`

```JSON
{
    "message": "Users not found"
}
```

# POST /users/token

## Description

Takes the username and password of a user to log in. If the credentials are valid, returns a JSON web token to be used for further authorization.

# Request Headers

```JSON
None
```

## Request Parameters

```
None
```

## Request Body

```
{
    username: 'example_username',
    password: 'example_password'
}
```

## Request Body Format

```
application/json
```

## Request JSON Attributes

| Name     | Type   | Description             | Required? |
| -------- | ------ | ----------------------- | --------- |
| username | string | username of the account | yes       |
| password | string | password of the account | yes       |

## Response Body Format

```
application/json
```

## Response Status Codes

| Outcome | Status Code       | Notes                                                                                                                 |
| ------- | ----------------- | --------------------------------------------------------------------------------------------------------------------- |
| Success | `201 Created`     |                                                                                                                       |
| Failure | `404 Not Found`   | If the username does not exist in the database at all.                                                                |
| Failure | `400 Bad Request` | The username exists in the database, but it does not match with the given password, or the provided data was invalid. |

## Response Body Examples

### Success - `201 Created`

```JSON
{
    "Token": "eyjHbgciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6InRlc3R1c2VybmFtZSIsInVzZXJfaWQiOjkzLCJpYXQiOjE2MDcxNDE5NjN9.rR8FJYXMDYqekez3M5uPEsLsu7wZM_j4Nb-OvFg9RFw",
    "user_id": 93
}
```

### Failure - `404 Not Found`

```JSON
{
    "message": "User not found"
}
```

### Failure - `400 Bad Request`

```JSON
{
    "type": "body",
    "message": "ValidationError: \"password\" is required"
}
```

## Boats

# POST /boats

## Description

Create a new boat. Requires authentication and sets the current user as the boat owner. Requires the user to be authenticated and pass a JSON Web Token.

# Request Headers

```JSON
{
    "Authorization": "Bearer eyjHbgciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6InRlc3R1c2VybmFtZSIsInVzZXJfaWQiOjkzLCJpYXQiOjE2MDcxNDE5NjN9.rR8FJYXMDYqekez3M5uPEsLsu7wZM_j4Nb-OvFg9RFw"
}
```

## Request Parameters

```
None
```

## Request Body

```JSON
{
    "boat_name": "example_boat_name",
    "boat_type": "example_boat_type",
    "length": 100
}
```

## Request Body Format

```
application/json
```

## Request JSON Attributes

| Name      | Type   | Description                    | Required? |
| --------- | ------ | ------------------------------ | --------- |
| boat_name | string | name of the boat               | yes       |
| boat_type | string | a description the type of boat | yes       |
| length    | int    | the length of the boat         | yes       |

## Response Body Format

```
application/json
```

## Response Status Codes

| Outcome | Status Code        | Notes                                  |
| ------- | ------------------ | -------------------------------------- |
| Success | `201 Created`      | boat created successfully              |
| Failure | `400 Bad Request`  | Invalid data given in request body     |
| Failure | `401 Unauthorized` | Authorization token invalid or missing |

## Response Body Examples

### Success - `201 Created`

```JSON
{
    "boat_name": "testboat",
    "boat_type": "sail boat",
    "length": 40,
    "owner_id": 555,
    "boat_id": 377,
    "self": "http://localhost:3000/boats/377"
}
```

### Failure - `400 Bad Request`

```JSON
{
    "type": "body",
    "message": "ValidationError: \"boat_type\" is required. \"length\" is required"
}
```

### Failure - `401 Unauthorized`

```JSON
{
    "message": "Authentication required"
}
```

# GET /boats

## Description

Gets a list of all boats that the user is the owner of. Fails if they are not authorized.

# Request Headers

```JSON
{
    "Authorization": "Bearer eyjHbgciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6InRlc3R1c2VybmFtZSIsInVzZXJfaWQiOjkzLCJpYXQiOjE2MDcxNDE5NjN9.rR8FJYXMDYqekez3M5uPEsLsu7wZM_j4Nb-OvFg9RFw"
}
```

## Request Parameters

```
None
```

## Request Body

```
None
```

## Request Body Format

```
None
```

## Request JSON Attributes

```
None
```

## Response Body Format

```
application/json
```

## Response Status Codes

| Outcome | Status Code        | Notes                                                                             |
| ------- | ------------------ | --------------------------------------------------------------------------------- |
| Success | `200 OK`           | returns a list of all boats the current user has access to                        |
| Failure | `401 Unauthorized` | user is not authorized or provided an invalid jwt                                 |
| Failure | `404 Not Found`    | fails when there are no boats (or public boats if not authorized) in the database |

## Response Body Examples

### Success - `200 OK`

```JSON
{
    "boats": [
        {
            "boat_id": 376,
            "owner_id": 555,
            "boat_name": "testboat",
            "boat_type": "sail boat",
            "length": 40,
            "loads": [],
            "self": "http://localhost:3000/boats/376"
        },
        {
            "boat_id": 377,
            "owner_id": 555,
            "boat_name": "testboat",
            "boat_type": "sail boat",
            "length": 40,
            "loads": [],
            "self": "http://localhost:3000/boats/377"
        }
    ],
    "count": 2,
    "next": "NO MORE PAGES"
}
```

### Failure - `401 Unauthorized`

```JSON
{
    "message": "Authentication required"
}
```

### Failure - `404 Not Found

```JSON
{
    "message": "User has no boats"
}
```

# GET /boats/:boat_id

## Description

Gets a single boat by id. Rejected if the boat is not public and the user is not the owner.

# Request Headers

```JSON
{
    "Authorization": "Bearer eyjHbgciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6InRlc3R1c2VybmFtZSIsInVzZXJfaWQiOjkzLCJpYXQiOjE2MDcxNDE5NjN9.rR8FJYXMDYqekez3M5uPEsLsu7wZM_j4Nb-OvFg9RFw"
}
```

## Request Parameters

| Name    | Type | Description                   | Required? |
| ------- | ---- | ----------------------------- | --------- |
| boat_id | int  | id of the boat to be returned | yes       |

## Request Body

```
None
```

## Request Body Format

```
None
```

## Request JSON Attributes

```
None
```

## Response Body Format

```
application/json
```

## Response Status Codes

| Outcome | Status Code        | Notes                                                           |
| ------- | ------------------ | --------------------------------------------------------------- |
| Success | `200 OK`           | boat with boat_id exists and the user has permission to view it |
| Failure | `401 Unauthorized` | Boat exists, is private, and the user is not authorized         |
| Failure | `403 Forbidden`    | Boat exists but the user does not have permission to view it    |
| Failure | `404 Not Found`    | boat with boat_id does not exist                                |

## Response Body Examples

### Success - `200 OK`

```JSON
{
    "boat_id": 377,
    "owner_id": 555,
    "boat_name": "testboat",
    "boat_type": "sail boat",
    "length": 40,
    "loads": [
        32,
        45,
        46
    ],
    "self": "http://localhost:3000/boats/377"
}
```

### Failure - `401 Unauthorized`

```JSON
{
    "message": "Authentication required"
}
```

### Failure - `403 Forbidden`

```JSON
{
    "message": "you do not own this boat"
}
```

### Failure - `404 Not Found`

```JSON
{
    "message": "boat not found"
}
```

# POST /boats/:boat_id/loads/:load_id

## Description

Add the load with load_id to the boat with boat_id. Only works if both boat_id and load_id correspond to existing entities, and if the user is authenticated and the owner of the boat. Also requires the load to not already be in another boat.

# Request Headers

```JSON
{
    "Authorization": "Bearer eyjHbgciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6InRlc3R1c2VybmFtZSIsInVzZXJfaWQiOjkzLCJpYXQiOjE2MDcxNDE5NjN9.rR8FJYXMDYqekez3M5uPEsLsu7wZM_j4Nb-OvFg9RFw"
}
```

## Request Parameters

| Name    | Type | Description                            | Required? |
| ------- | ---- | -------------------------------------- | --------- |
| boat_id | int  | id of the boat                         | yes       |
| load_id | int  | id of the load to be added to the boat | yes       |

## Request Body

```
None
```

## Request Body Format

```
None
```

## Request JSON Attributes

```
None
```

## Response Body Format

```
None
```

## Response Status Codes

| Outcome | Status Code        | Notes                                  |
| ------- | ------------------ | -------------------------------------- |
| Success | `204 No Content`   |                                        |
| Failure | `400 Bad Request`  | the load is already in a boat          |
| Failure | `401 Unauthorized` | user is not authenticated              |
| Failure | `403 Forbidden`    | user does not own the boat             |
| Failure | `404 Not Found`    | either the boat or load does not exist |

## Response Body Examples

### Success - `204 No Content`

```JSON

```

### Failure - `400 Bad Request`

```JSON
{
    "message": "load already loaded"
}
```

### Failure - `401 Unauthorized`

```JSON
{
    "message": "Authentication required"
}
```

### Failure - `403 Forbidden`

```JSON
{
    "message": "you do not own this boat"
}
```

### Failure - `404 Not Found`

```JSON
{
    "message": "boat not found"
}
```

# DELETE /boats/:boat_id/loads/:load_id

## Description

Remove the load with load_id from the boat with boat_id. Both entities must exist and the load must be in the boat.

# Request Headers

```JSON
{
    "Authorization": "Bearer eyjHbgciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6InRlc3R1c2VybmFtZSIsInVzZXJfaWQiOjkzLCJpYXQiOjE2MDcxNDE5NjN9.rR8FJYXMDYqekez3M5uPEsLsu7wZM_j4Nb-OvFg9RFw"
}
```

## Request Parameters

| Name    | Type | Description                            | Required? |
| ------- | ---- | -------------------------------------- | --------- |
| boat_id | int  | id of the boat                         | yes       |
| load_id | int  | id of the load to be added to the boat | yes       |

## Request Body

```
None
```

## Request Body Format

```
None
```

## Request JSON Attributes

```
None
```

## Response Body Format

```
application/json
```

## Response Status Codes

| Outcome | Status Code        | Notes                                  |
| ------- | ------------------ | -------------------------------------- |
| Success | `204 No Content`   |                                        |
| Failure | `400 Bad Request`  | the load is not in the boat            |
| Failure | `401 Unauthorized` | the user is not authenticated          |
| Failure | `403 Forbidden`    | user does not own the boat             |
| Failure | `404 Not Found`    | either the boat or load does not exist |

## Response Body Examples

### Success - `204 No Content`

```JSON

```

### Failure - `400 Bad Request`

```JSON
{
    "message": "load not loaded"
}
```

### Failure - `401 Unauthorized`

```JSON
{
    "message": "Authentication required"
}
```

### Failure - `403 Forbidden`

```JSON
{
    "message": "you do not own this boat"
}
```

### Failure - `404 Not Found`

```JSON
{
    "message": "boat not found"
}
```

# GET /boats/:boat_id/loads/

## Description

Get a list of loads that are carried by the given boat. User must have permission to view the boat.

# Request Headers

```JSON
{
    "Authorization": "Bearer eyjHbgciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6InRlc3R1c2VybmFtZSIsInVzZXJfaWQiOjkzLCJpYXQiOjE2MDcxNDE5NjN9.rR8FJYXMDYqekez3M5uPEsLsu7wZM_j4Nb-OvFg9RFw"
}
```

## Request Parameters

| Name    | Type | Description    | Required? |
| ------- | ---- | -------------- | --------- |
| boat_id | int  | id of the boat | yes       |

## Request Body

```
None
```

## Request Body Format

```
None
```

## Request JSON Attributes

```
None
```

## Response Body Format

```
application/json
```

## Response Status Codes

| Outcome | Status Code        | Notes                                           |
| ------- | ------------------ | ----------------------------------------------- |
| Success | `200 OK`           |                                                 |
| Failure | `401 Unauthorized` | user is not authorized                          |
| Failure | `403 Forbidden`    | user does not have permission to view this boat |
| Failure | `404 Not Found`    | boat does not exist                             |

## Response Body Examples

### Success - `200 OK`

```JSON
[
    {
        "deliver_by": "2021-01-19T00:00:00.000Z",
        "description": "testdescription",
        "load_id": 548,
        "weight": 40,
        "boat_id": 377,
        "self": "http://localhost:3000/loads/548"
    }
]
```

### Failure - `401 Unauthorized`

```JSON
{
    "message": "Authentication required"
}
```

### Failure - `403 Forbidden`

```JSON
{
    "message": "you do not own this boat"
}
```

### Failure - `404 Not Found`

```JSON
{
    "message": "boat not found"
}
```

# PATCH /boats/:boat_id

## Description

Modify a boat taking in only the properties that are to be modified.

# Request Headers

```JSON
{
    "Authorization": "Bearer eyjHbgciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6InRlc3R1c2VybmFtZSIsInVzZXJfaWQiOjkzLCJpYXQiOjE2MDcxNDE5NjN9.rR8FJYXMDYqekez3M5uPEsLsu7wZM_j4Nb-OvFg9RFw"
}
```

## Request Parameters

| Name    | Type | Description    | Required? |
| ------- | ---- | -------------- | --------- |
| boat_id | int  | id of the boat | yes       |

## Request Body

```JSON
{
    "name": "updated_name"
}
```

```JSON
{
    "type": "updated_type"
}
```

```JSON
{
    "name": "updated_name",
    "type": "updated_type"
}
```

## Request Body Format

```
application/json
```

## Request JSON Attributes

| Name   | Type   | Description        | Required? |
| ------ | ------ | ------------------ | --------- |
| name   | string | name of the boat   | optional  |
| type   | string | type of the boat   | optional  |
| length | int    | length of the boat | optional  |

## Response Body Format

```
application/json
```

## Response Status Codes

| Outcome | Status Code        | Notes                                        |
| ------- | ------------------ | -------------------------------------------- |
| Success | `204 No Content`   |                                              |
| Failure | `400 Bad Request`  | no properties were sent                      |
| Failure | `401 Unauthorized` | the user is not authorized                   |
| Failure | `403 Forbidden`    | user is authorized but does now own the boat |
| Failure | `404 Not Found`    | Boat does not exist                          |

## Response Body Examples

### Success - `204 No Content`

```JSON

```

### Failure - `400 Bad Request`

```JSON
{
    "type": "body",
    "message": "ValidationError: \"value\" must contain at least one of [boat_name, boat_type, length]"
}
```

### Failure - `401 Unauthorized`

```JSON
{
    "message": "Authentication required"
}
```

### Failure - `403 Forbidden`

```JSON
{
    "message": "you do not own this boat"
}
```

### Failure - `404 Not Found`

```JSON
{
    "message": "boat not found"
}
```

# PUT /boats/:boat_id

## Description

Replace the underlying data for a boat with a completely new boat. Maintains boat_id, owner_id, and the loads array while changing everything else.

# Request Headers

```JSON
{
    "Authorization": "Bearer eyjHbgciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6InRlc3R1c2VybmFtZSIsInVzZXJfaWQiOjkzLCJpYXQiOjE2MDcxNDE5NjN9.rR8FJYXMDYqekez3M5uPEsLsu7wZM_j4Nb-OvFg9RFw"
}
```

## Request Parameters

| Name    | Type | Description    | Required? |
| ------- | ---- | -------------- | --------- |
| boat_id | int  | id of the boat | yes       |

## Request Body

```JSON
{
    "name": "updated_name",
    "type": "updated_type",
    "length": 20
}
```

## Request Body Format

```
application/json
```

## Request JSON Attributes

| Name   | Type   | Description        | Required? |
| ------ | ------ | ------------------ | --------- |
| name   | string | name of the boat   | yes       |
| type   | string | type of the boat   | yes       |
| length | int    | length of the boat | yes       |

## Response Body Format

```
application/json
```

## Response Status Codes

| Outcome | Status Code        | Notes                         |
| ------- | ------------------ | ----------------------------- |
| Success | `204 No Content`   |                               |
| Failure | `400 Bad Request`  | Invalid or missing properties |
| Failure | `401 Unauthorized` | user is not authorized        |
| Failure | `403 Forbidden`    | user does not own the boat    |
| Failure | `404 Not Found`    | boat does not exist           |

## Response Body Examples

### Success - `204 No Conent`

```JSON

```

### Failure - `400 Bad Request`

```JSON
{
    "type": "body",
    "message": "ValidationError: \"length\" is required"
}
```

### Failure - `401 Unauthorized`

```JSON
{
    "message": "Authentication required"
}
```

### Failure - `403 Forbidden`

```JSON
{
    "message": "you do not own this boat"
}
```

### Failure - `404 Not Found`

```JSON
{
    "message": "boat not found"
}
```

# DELETE /boats/:boat_id

## Description

Deletes a boat if the user is authorized to do so and it exists.

# Request Headers

```JSON
{
    "Authorization": "Bearer eyjHbgciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6InRlc3R1c2VybmFtZSIsInVzZXJfaWQiOjkzLCJpYXQiOjE2MDcxNDE5NjN9.rR8FJYXMDYqekez3M5uPEsLsu7wZM_j4Nb-OvFg9RFw"
}
```

## Request Parameters

```
None
```

## Request Body

```
None
```

## Request Body Format

```
None
```

## Request JSON Attributes

```
None
```

## Response Body Format

```
application/json
```

## Response Status Codes

| Outcome | Status Code        | Notes |
| ------- | ------------------ | ----- |
| Success | `204 No Content`   |       |
| Failure | `401 Unauthorized` |       |
| Failure | `403 Forbidden`    |       |
| Failure | `404 Not Found`    |       |

## Response Body Examples

### Success - `204 No Conent`

```JSON

```

### Failure - `401 Unauthorized`

```JSON
{
    "message": "Authentication required"
}
```

### Failure - `403 Forbidden`

```JSON
{
    "message": "you do not own this boat"
}
```

### Failure - `404 Not Found`

```JSON
{
    "message": "boat not found"
}
```

## Loads

# POST /loads

## Description

Create a new load.

# Request Headers

```JSON
{
    "Authorization": "Bearer eyjHbgciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6InRlc3R1c2VybmFtZSIsInVzZXJfaWQiOjkzLCJpYXQiOjE2MDcxNDE5NjN9.rR8FJYXMDYqekez3M5uPEsLsu7wZM_j4Nb-OvFg9RFw"
}
```

## Request Parameters

```
None
```

## Request Body

```JSON
{
    "description": "example description",
    "weight": 50,
    "deliver_by": "2021-01-20"
}
```

## Request Body Format

```
application/json
```

## Request JSON Attributes

| Name        | Type   | Description                                | Required? |
| ----------- | ------ | ------------------------------------------ | --------- |
| description | string | a description of the load                  | yes       |
| weight      | int    | the weight of the load                     | yes       |
| deliver_by  | date   | the date the load needs to be delivered by | yes       |

## Response Body Format

```
application/json
```

## Response Status Codes

| Outcome | Status Code        | Notes                       |
| ------- | ------------------ | --------------------------- |
| Success | `201 Created`      |                             |
| Failure | `400 Bad Request`  | Invalid or missing property |
| Failure | `401 Unauthorized` | User is not authorized      |

## Response Body Examples

### Success - `201 Created`

```JSON
{
    "description": "test",
    "weight": 40,
    "deliver_by": "2020-02-20",
    "load_id": 551,
    "boat_id": null,
    "self": "http://localhost:3000/loads/551"
}
```

### Failure - `400 Bad Request`

```JSON
{
    "type": "body",
    "message": "ValidationError: \"description\" is required. \"weight\" is required. \"deliver_by\" is required. \"boat_name\" is not allowed. \"boat_type\" is not allowed. \"length\" is not allowed"
}
```

### Failure - `401 Unauthorized`

```JSON
{
    "message": "Authentication required"
}
```

# GET /loads

## Description

get all loads.

# Request Headers

```JSON
{
    "Authorization": "Bearer eyjHbgciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6InRlc3R1c2VybmFtZSIsInVzZXJfaWQiOjkzLCJpYXQiOjE2MDcxNDE5NjN9.rR8FJYXMDYqekez3M5uPEsLsu7wZM_j4Nb-OvFg9RFw"
}
```

## Request Parameters

```
None
```

## Request Body

```
None
```

## Request Body Format

```
None
```

## Request JSON Attributes

```
None
```

## Response Body Format

```
application/json
```

## Response Status Codes

| Outcome | Status Code        | Notes               |
| ------- | ------------------ | ------------------- |
| Success | `200 OK`           |                     |
| Failure | `401 Unauthorized` | user not authorized |
| Failure | `404 Not Found`    | db has no loads     |

## Response Body Examples

### Success - `200 OK`

```JSON
{
    "loads": [
        {
            "deliver_by": "2020-12-20T00:00:00.000Z",
            "description": "test",
            "load_id": 2,
            "weight": 40,
            "boat_id": null,
            "self": "http://localhost:3000/loads/2"
        },
        {
            "deliver_by": "2020-12-20T00:00:00.000Z",
            "description": "test",
            "load_id": 3,
            "weight": 40,
            "boat_id": null,
            "self": "http://localhost:3000/loads/3"
        },
        {
            "deliver_by": "2020-12-20T00:00:00.000Z",
            "description": "test",
            "load_id": 4,
            "weight": 40,
            "boat_id": null,
            "self": "http://localhost:3000/loads/4"
        },
        {
            "deliver_by": "2020-12-20T00:00:00.000Z",
            "description": "test",
            "load_id": 5,
            "weight": 40,
            "boat_id": null,
            "self": "http://localhost:3000/loads/5"
        },
        {
            "deliver_by": "2020-12-16T00:00:00.000Z",
            "description": "test1",
            "load_id": 6,
            "weight": 6969,
            "boat_id": null,
            "self": "http://localhost:3000/loads/6"
        }
    ],
    "count": 384,
    "next": "http://localhost:3000/loads?page_size=5&page=1"
}
```

### Failure - `401 Unauthorized`

```JSON
{
    "message": "Authentication required"
}
```

### Failure - `404 Not Found`

I was unable to test this with the postman collection as there is no way to guarentee there will not be loads in the db when this is run.

```JSON
{
    "message": "No loads found"
}
```

# GET /loads/:load_id

## Description

get an individual load by id

# Request Headers

```JSON
{
    "Authorization": "Bearer eyjHbgciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6InRlc3R1c2VybmFtZSIsInVzZXJfaWQiOjkzLCJpYXQiOjE2MDcxNDE5NjN9.rR8FJYXMDYqekez3M5uPEsLsu7wZM_j4Nb-OvFg9RFw"
}
```

## Request Parameters

| Name    | Type | Description    | Required? |
| ------- | ---- | -------------- | --------- |
| load_id | int  | id of the load | yes       |

## Request Body

```
None
```

## Request Body Format

```
None
```

## Request JSON Attributes

```
None
```

## Response Body Format

```
application/json
```

## Response Status Codes

| Outcome | Status Code        | Notes                  |
| ------- | ------------------ | ---------------------- |
| Success | `200 OK `          |                        |
| Failure | `401 Unauthorized` | user is not authorized |
| Failure | `404 Not Found`    | load does not exist    |

## Response Body Examples

### Success - `200 OK`

```JSON
{
    "deliver_by": "2021-01-19T00:00:00.000Z",
    "description": "testdescription",
    "load_id": 550,
    "weight": 40,
    "boat_id": null,
    "self": "http://localhost:3000/loads/550"
}
```

### Failure - `401 Unauthorized`

```JSON
{
    "message": "Authentication required"
}
```

### Failure - `404 Not Found`

```JSON
{
    "message": "load not found"
}
```

# PATCH /loads/:load_id

## Description

Modify a load taking in only the properties that are to be modified.

# Request Headers

```JSON
{
    "Authorization": "Bearer eyjHbgciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6InRlc3R1c2VybmFtZSIsInVzZXJfaWQiOjkzLCJpYXQiOjE2MDcxNDE5NjN9.rR8FJYXMDYqekez3M5uPEsLsu7wZM_j4Nb-OvFg9RFw"
}
```

## Request Parameters

| Name    | Type | Description    | Required? |
| ------- | ---- | -------------- | --------- |
| load_id | int  | id of the load | yes       |

## Request Body

```JSON
{
    "description": "updated_description"
}
```

```JSON
{
    "weight": 39
}
```

```JSON
{
    "description": "updated_description",
    "weight": 39
}
```

## Request Body Format

```
application/json
```

## Request JSON Attributes

| Name        | Type   | Description                            | Required? |
| ----------- | ------ | -------------------------------------- | --------- |
| description | string | description of the load                | optional  |
| weight      | int    | weight of the load                     | optional  |
| deliver_by  | date   | date the load needs to be delivered by | optional  |

## Response Body Format

```
application/json
```

## Response Status Codes

| Outcome | Status Code        | Notes                                    |
| ------- | ------------------ | ---------------------------------------- |
| Success | `204 No Content`   |                                          |
| Failure | `400 Bad Request`  | Invalid properties or no properties sent |
| Failure | `401 Unauthorized` | User is not authorized                   |
| Failure | `404 Not Found`    | the load does not exist                  |

## Response Body Examples

### Success - `204 No Content`

```JSON

```

### Failure - `400 Bad Request`

```JSON
{
    "type": "body",
    "message": "ValidationError: \"value\" must contain at least one of [description, weight, deliver_by]"
}
```

### Failure - `401 Unauthorized`

```JSON
{
    "message": "Authentication required"
}
```

### Failure - `404 Not Found`

```JSON
{
    "message": "load not found"
}
```

# PUT /loads/:load_id

## Description

Fully replace the underlying data for a given load. Maintains load_id

# Request Headers

```JSON
{
    "Authorization": "Bearer eyjHbgciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6InRlc3R1c2VybmFtZSIsInVzZXJfaWQiOjkzLCJpYXQiOjE2MDcxNDE5NjN9.rR8FJYXMDYqekez3M5uPEsLsu7wZM_j4Nb-OvFg9RFw"
}
```

## Request Parameters

| Name    | Type | Description    | Required? |
| ------- | ---- | -------------- | --------- |
| load_id | int  | id of the load | yes       |

## Request Body

```JSON
{
    "description": "updated_description",
    "weight": 39,
    "deliver_by": "2021-02-19"
}
```

## Request Body Format

```
application/json
```

## Request JSON Attributes

| Name        | Type   | Description                            | Required? |
| ----------- | ------ | -------------------------------------- | --------- |
| description | string | description of the load                | yes       |
| weight      | int    | weight of the load                     | yes       |
| deliver_by  | date   | date the load needs to be delivered by | yes       |

## Response Body Format

```
application/json
```

## Request JSON Attributes

| Name        | Type   | Description                            | Required? |
| ----------- | ------ | -------------------------------------- | --------- |
| description | string | description of the load                | optional  |
| weight      | int    | weight of the load                     | optional  |
| deliver_by  | date   | date the load needs to be delivered by | optional  |

## Response Body Format

```
application/json
```

## Response Status Codes

| Outcome | Status Code        | Notes                         |
| ------- | ------------------ | ----------------------------- |
| Success | `204 No Content`   |                               |
| Failure | `400 Bad Request`  | Invalid or missing properties |
| Failure | `401 Unauthorized` | User is not authorized        |
| Failure | `404 Not Found`    | the load does not exist       |

## Response Body Examples

### Success - `204 No Content`

```JSON

```

### Failure - `400 Bad Request`

```JSON
{
    "type": "body",
    "message": "ValidationError: \"description\" is required"
}
```

### Failure - `401 Unauthorized`

```JSON
{
    "message": "Authentication required"
}
```

### Failure - `404 Not Found`

```JSON
{
    "message": "load not found"
}
```

# DELETE /loads/:load_id

## Description

Delete the boat with the given id. I didn't implement an owner_id for the loads, so a load can be deleted by anybody. This is something that would need to change to use an app like this in production but is not difficult to add.

# Request Headers

```JSON
{
    "Authorization": "Bearer eyjHbgciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6InRlc3R1c2VybmFtZSIsInVzZXJfaWQiOjkzLCJpYXQiOjE2MDcxNDE5NjN9.rR8FJYXMDYqekez3M5uPEsLsu7wZM_j4Nb-OvFg9RFw"
}
```

## Request Parameters

| Name    | Type | Description    | Required? |
| ------- | ---- | -------------- | --------- |
| load_id | int  | id of the load | yes       |

## Request Body

```
None
```

## Request Body Format

```
None
```

## Request JSON Attributes

```
None
```

## Response Body Format

```
application/json
```

## Response Status Codes

| Outcome | Status Code        | Notes                   |
| ------- | ------------------ | ----------------------- |
| Success | `204 No Content`   |                         |
| Failure | `401 Unauthorized` | User is not authorized  |
| Failure | `404 Not Found`    | the load does not exist |

## Response Body Examples

### Success - `204 No Content`

```JSON

```

### Failure - `401 Unauthorized`

```JSON
{
    "message": "Authentication required"
}
```

### Failure - `404 Not Found`

```JSON
{
    "message": "load not found"
}
```
